package com.alirnp.beeptunes.repository.api;

import com.alirnp.beeptunes.model.Result;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.PUT;

public interface CurrencyApi {

    @GET("api")
    Observable<Response<Result>> getCurrencies();
}
