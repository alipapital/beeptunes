package com.alirnp.beeptunes.repository.api;

public class Config {

   /**
    * URL of https requests
    */
   public static final String BASE_URL = "http://beeptunes.ca:8088/";
}
