package com.alirnp.beeptunes.repository;

import com.alirnp.beeptunes.model.Result;
import com.alirnp.beeptunes.repository.api.CurrencyApi;
import com.alirnp.beeptunes.repository.api.RetrofitClient;

import io.reactivex.Observable;
import retrofit2.Response;

public class CurrencyRepository {

    private final CurrencyApi currencyApi;

    public CurrencyRepository() {
        currencyApi = RetrofitClient.getApiClient().getCurrencyApi();
    }

    public Observable<Response<Result>> getCurrencyList(){
        return currencyApi.getCurrencies();
    }
}
