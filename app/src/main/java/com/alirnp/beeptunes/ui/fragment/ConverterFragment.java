package com.alirnp.beeptunes.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import com.alirnp.beeptunes.R;
import com.alirnp.beeptunes.databinding.FragmentConverterBinding;
import com.alirnp.beeptunes.model.Currency;
import com.alirnp.beeptunes.model.ResponseHolder;
import com.alirnp.beeptunes.model.Result;
import com.alirnp.beeptunes.ui.adapter.CurrencyAdapter;
import com.alirnp.beeptunes.viewModel.CurrencyViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;


public class ConverterFragment extends Fragment {

    private static final String TAG = "LOG_ME";
    private Context context;
    private FragmentConverterBinding binding;
    private CurrencyViewModel mCurrencyViewModel;
    private CurrencyAdapter mAdapter;

    public ConverterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_converter, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new CurrencyAdapter();
        binding.recyclerView.setAdapter(mAdapter);

        mCurrencyViewModel = new CurrencyViewModel();
        mCurrencyViewModel.getResponseHolder().observe((LifecycleOwner) context, this::onStateChanged);

        getCurrencyEverySecond();
    }

    private void getCurrencyEverySecond() {
        Observable
                .interval(0,5, TimeUnit.SECONDS)
                .doOnNext(n -> mCurrencyViewModel.getCurrencies())
                .subscribe();
    }

    /**
     * @param response is status of request
     */
    private void onStateChanged(ResponseHolder response) {

        switch (response.status) {

            case LOADING:
                Log.i(TAG, "onStateChanged: LOADING");
                break;
            case SUCCESS:
                Log.i(TAG, "onStateChanged: SUCCESS |");
                showItemsInRecyclerView(response.result);
                break;
            case ERROR:
                Log.i(TAG, "onStateChanged: ERROR | " + response.error);
                Toast.makeText(context, response.error.getMessage(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /**
     * @param result received from response of server
     */
    private void showItemsInRecyclerView(Result result) {
        List<Currency> items = mCurrencyViewModel.getCurrencyItemsFromResult(result);
        mAdapter.setItems(items);
    }
}