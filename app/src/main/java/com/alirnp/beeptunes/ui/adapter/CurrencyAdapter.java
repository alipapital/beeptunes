package com.alirnp.beeptunes.ui.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alirnp.beeptunes.R;
import com.alirnp.beeptunes.common.CurrencyHelper;
import com.alirnp.beeptunes.model.Currency;
import com.alirnp.beeptunes.ui.CustomEditText;

import java.util.ArrayList;
import java.util.List;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyHolder> {

    public List<Currency> items = new ArrayList<>();
    public Currency itemFocused;
    private int positionOfCursor;
    private LayoutInflater mLayoutInflater;
    private RecyclerView mRecyclerView;
    private double scaleEUR = 1;

    public CurrencyAdapter() {/**/}

    /**
     * @param items Included Curency list
     */
    public void setItems(List<Currency> items) {

        if (itemFocused != null) {
            int position = getItemPositionInList(itemFocused, items);

            if (position != -1) {
                items.remove(position);
                items.add(0, itemFocused);
                items.get(0).setPositionOfCursor(positionOfCursor);
            }
        }

        this.items = items;

        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
    }

    /**
     * @return position of currency
     */
    private int getItemPositionInList(Currency currency, List<Currency> items) {
        for (int i = 0; i < items.size(); i++) {
            String codeOfItems = items.get(i).getCode();
            String codeOfCurrency = currency.getCode();

            boolean found = (codeOfItems.equals(codeOfCurrency));
            if (found) {
                return i;
            }
        }
        return -1;
    }

    /**
     * move item focused to first element
     * @param position old position of currency
     */
    private void moveItemToFirst(int position) {
        if (position == -1 || position == 0) return;

        Currency currency = items.get(position);

        itemFocused = currency;

        items.remove(position);
        items.add(0, currency);
        items.get(0).setFocused(true);

        notifyItemMoved(position, 0);
    }


    @NonNull
    @Override
    public CurrencyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mLayoutInflater == null)
            mLayoutInflater = LayoutInflater.from(parent.getContext());

        View view = mLayoutInflater.inflate(R.layout.item_currency, parent, false);
        return new CurrencyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyHolder holder, int position) {
        holder.bindCurrency(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public class CurrencyHolder extends RecyclerView.ViewHolder {
        private final TextView mTextViewCode;
        private final CustomEditText mEditTextValue;
        private TextWatcher mTextWatcher;
        private final ImageView mImageViewFlag;

        public CurrencyHolder(@NonNull View itemView) {
            super(itemView);
            mTextViewCode = itemView.findViewById(R.id.textView_code);
            mEditTextValue = itemView.findViewById(R.id.editText_value);
            mImageViewFlag = itemView.findViewById(R.id.imageView_flag);

            mEditTextValue.setOnFocusChangeListener(this::onFocusChangeListener);
            mEditTextValue.setTag(false);
            mEditTextValue.addTextChangedListener(getTextChanged());
            mEditTextValue.setOnSelectionChanged(this::onSelectionChange);
        }

        private void onSelectionChange(int selStart, int selEnd) {
            if (selEnd > 0) {
                positionOfCursor = selEnd;
            }

        }

        private void onFocusChangeListener(View v, boolean hasFocus) {
            mEditTextValue.setTag(true);
            moveItemToFirst(getAdapterPosition());
        }

        private TextWatcher getTextChanged() {
            if (mTextWatcher == null) {
                mTextWatcher = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (getAdapterPosition() == -1) return;

                        String input = s.toString();

                        if (isNumeric(input) && hasInput(input) && isFocused(mEditTextValue)) {
                            Currency currency = items.get(getAdapterPosition());
                            double baseCurrency = currency.getValue();
                            scaleEUR = calculateScale(input, baseCurrency);
                            if (itemFocused != null) {
                                itemFocused.setValue(Double.parseDouble(input) / scaleEUR);
                            }

                            mRecyclerView.post((Runnable) () -> notifyItemRangeChanged(1, items.size() - 1));

                        }
                    }
                };
            }
            return mTextWatcher;
        }

        /**
         * @return the string is number or not
         */
        public boolean isNumeric(String strNum) {
            if (strNum == null) {
                return false;
            }
            try {
                double d = Double.parseDouble(strNum);
            } catch (NumberFormatException nfe) {
                return false;
            }
            return true;
        }

        private boolean isFocused(TextView editText) {
            return (Boolean) editText.getTag();
        }

        /**
         * @param input the value of editText
         * @return editText is empty or not
         */
        private boolean hasInput(CharSequence input) {
            return input != null && input.length() > 0;
        }

        /**
         * @param input value of currency
         * @param baseCurrency base currency received from response
         * @return scaled currency by base currency
         */
        private double calculateScale(CharSequence input, double baseCurrency) {
            double inputCurrency = Double.parseDouble(input.toString());
            return inputCurrency / baseCurrency;
        }

        /**
         * @return name of currency
         */
        private String getNameOfCurrency(Currency currency) {
            String name = currency.getCode();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                name = String.format("%s(%s)", CurrencyHelper.getCurrencyName(currency), currency.getCode());
            }

            return name;
        }

        /**
         * @param value currency value
         * @return round the currency value
         */
        private String getRoundedValue(double value) {
            double roundedValue = value * scaleEUR;
            roundedValue = Math.round(roundedValue * 100.000) / 100.000;
            return String.valueOf(roundedValue);
        }

        void bindCurrency(Currency currency) {
            mTextViewCode.setText(getNameOfCurrency(currency));
            mImageViewFlag.setImageResource(CurrencyHelper.getCurrencyFlag(currency));
            mEditTextValue.setText(getRoundedValue(currency.getValue()));

            if (currency.isFocused()) {
                int cursorPosition = currency.getPositionOfCursor();
                int sizeOfEditTextValue = mEditTextValue.getText().length();
                if (cursorPosition < sizeOfEditTextValue) {
                    mEditTextValue.setSelection(cursorPosition);
                }
            }
        }
    }
}
