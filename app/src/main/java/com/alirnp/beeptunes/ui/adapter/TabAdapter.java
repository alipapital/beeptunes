package com.alirnp.beeptunes.ui.adapter;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class TabAdapter extends FragmentStatePagerAdapter {
   private final List<Fragment> mFragmentList = new ArrayList<>();
   private final List<String> mFragmentTitleList = new ArrayList<>();

   public TabAdapter(FragmentManager fm) {
      super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
   }

   @NotNull
   @Override
   public Fragment getItem(int position) {
      return mFragmentList.get(position);
   }

   public void addFragment(Fragment fragment, String title) {
      mFragmentList.add(fragment);
      mFragmentTitleList.add(title);
   }

   public void replaceFragment(int position, Fragment fragment) {
      mFragmentList.remove(position);
      mFragmentList.add(position, fragment);
   }

   public void removeFragment(Fragment fragment) {
      mFragmentList.remove(fragment);
   }

   public void remove(int position) {
      mFragmentList.remove(position);
      mFragmentTitleList.remove(position);
   }

   @Nullable
   @Override
   public CharSequence getPageTitle(int position) {
      return mFragmentTitleList.get(position);
   }

   @Override
   public int getCount() {
      return mFragmentList.size();
   }
} 
