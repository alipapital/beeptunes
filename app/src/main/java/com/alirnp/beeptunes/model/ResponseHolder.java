package com.alirnp.beeptunes.model;

public class ResponseHolder {

    public final  Status status;
    public final Result result;
    public final Throwable error;

    public ResponseHolder(Status status, Result result, Throwable error) {
        this.status = status;
        this.result = result;
        this.error = error;
    }

    public static ResponseHolder loading(){
        return new ResponseHolder(Status.LOADING , null , null);
    }

    public static ResponseHolder success(Result result){
        return new ResponseHolder(Status.SUCCESS , result , null);
    }

    public static ResponseHolder error(Throwable throwable){
        return new ResponseHolder(Status.ERROR , null , throwable);
    }


}
