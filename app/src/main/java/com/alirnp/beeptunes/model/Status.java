package com.alirnp.beeptunes.model;

public enum Status {
    LOADING,
    SUCCESS,
    ERROR
}
