package com.alirnp.beeptunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

@SerializedName("baseCurrency")
@Expose
private String baseCurrency;
@SerializedName("rates")
@Expose
private Rates rates;

public String getBaseCurrency() {
return baseCurrency;
}

public void setBaseCurrency(String baseCurrency) {
this.baseCurrency = baseCurrency;
}

public Rates getRates() {
return rates;
}

public void setRates(Rates rates) {
this.rates = rates;
}

}