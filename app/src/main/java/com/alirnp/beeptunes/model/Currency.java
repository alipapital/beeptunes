package com.alirnp.beeptunes.model;

public class Currency {

    private final String code;
    private double value;
    private boolean focused;
    private int positionOfCursor;

    public Currency(String code, double value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public boolean isFocused() {
        return focused;
    }

    public void setFocused(boolean focused) {
        this.focused = focused;
    }

    public int getPositionOfCursor() {
        return positionOfCursor;
    }

    public void setPositionOfCursor(int positionOfCursor) {
        this.positionOfCursor = positionOfCursor;
    }

}
