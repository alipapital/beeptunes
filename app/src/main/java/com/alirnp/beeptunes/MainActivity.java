package com.alirnp.beeptunes;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.alirnp.beeptunes.databinding.ActivityMainBinding;
import com.alirnp.beeptunes.ui.adapter.TabAdapter;
import com.alirnp.beeptunes.ui.fragment.ConverterFragment;
import com.alirnp.beeptunes.ui.fragment.RatesAllFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());

        ConverterFragment converter = new ConverterFragment();
        RatesAllFragment ratesAll = new RatesAllFragment();

        adapter.addFragment(converter, "Converter");
        adapter.addFragment(ratesAll, "Rates All");

        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
    }
}