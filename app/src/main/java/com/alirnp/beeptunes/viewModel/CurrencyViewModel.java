package com.alirnp.beeptunes.viewModel;

import androidx.lifecycle.MutableLiveData;

import com.alirnp.beeptunes.model.Currency;
import com.alirnp.beeptunes.model.ResponseHolder;
import com.alirnp.beeptunes.model.Result;
import com.alirnp.beeptunes.repository.CurrencyRepository;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class CurrencyViewModel {

    private final CurrencyRepository mCurrencyRepository;
    private final MutableLiveData<ResponseHolder> mResponseHolder = new MutableLiveData<>();

    public CurrencyViewModel() {
        mCurrencyRepository = new CurrencyRepository();
    }

    /**
     * listen to Response changes
     * @return MutableLiveData of Response
     */
    public MutableLiveData<ResponseHolder> getResponseHolder(){
        return mResponseHolder;
    }

    /**
     * get list of currency from API and publish that with {@link #mResponseHolder}
     */
    public void getCurrencies() {
        mCurrencyRepository.getCurrencyList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<Result>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mResponseHolder.postValue(ResponseHolder.loading());
                    }

                    @Override
                    public void onNext(@NonNull Response<Result> response) {
                        if (response.isSuccessful())
                            mResponseHolder.postValue(ResponseHolder.success(response.body()));
                        else
                            mResponseHolder.postValue(ResponseHolder.error(new Throwable(response.message())));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mResponseHolder.postValue(ResponseHolder.error(e));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * convert result to list of currency
     * @param result get from {@link #mResponseHolder}
     * @return list of Currency
     */
    public List<Currency> getCurrencyItemsFromResult(Result result) {
        List<Currency> items = new ArrayList<>();

        String jsonInString = new Gson().toJson(result.getRates());
        JsonObject convertedObject = new Gson().fromJson(jsonInString, JsonObject.class);
        Set<Map.Entry<String, JsonElement>> entries = convertedObject.entrySet();

        for (Map.Entry<String, JsonElement> entry : entries) {
            String key = entry.getKey();
            double value = entry.getValue().getAsDouble();
            Currency currency = new Currency(key  , value);
            items.add(currency);
        }
        return items;
    }
}
