package com.alirnp.beeptunes.common;

import android.os.Build;

import androidx.annotation.DrawableRes;
import androidx.annotation.RequiresApi;

import com.alirnp.beeptunes.R;
import com.alirnp.beeptunes.model.Currency;

import java.util.Locale;

public class CurrencyHelper {

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getCurrencyName(Currency currency){
        Locale uk = new Locale("en", "GB");
        java.util.Currency instance =java.util.Currency.getInstance(currency.getCode());
        return instance.getDisplayName(uk);
    }

    public static @DrawableRes  int getCurrencyFlag(Currency currency){
        switch (currency.getCode()){
                    case "AUD":
                        return R.drawable.australia;

                    case "BGN":
                        return R.drawable.bulgaria;

                    case "BRL":
                        return R.drawable.brazil;

                    case "CAD":
                        return R.drawable.canada;

                    case "CHF":
                        return R.drawable.liechtenstein;

                    case "CNY":
                        return R.drawable.china;

                    case "CZK":
                        return R.drawable.czech_republic;

                    case "DKK":
                        return R.drawable.denmark;

                    case "GBP":
                        return R.drawable.guernsey;

                    case "HKD":
                        return R.drawable.hong_kong;

                    case "HRK":
                        return R.drawable.croatia;

                    case "HUF":
                        return R.drawable.hungary;

                    case "IDR":
                        return R.drawable.indonesia;

                    case "ILS":
                        return R.drawable.israel;

                    case "INR":
                        return R.drawable.india;

                    case "ISK":
                        return R.drawable.iceland;

                    case "JPY":
                        return R.drawable.japan;

                    case "KRW":
                        return R.drawable.north_korea;

                    case "MXN":
                        return R.drawable.mexico;

                    case "MYR":
                        return R.drawable.malaysia;

                    case "NOK":
                        return R.drawable.norway;

                    case "NZD":
                        return R.drawable.new_zealand;

                    case "PHP":
                        return R.drawable.philippines;

                    case "PLN":
                        return R.drawable.republic_of_poland;

                    case "RON":
                        return R.drawable.romania;

                    case "RUB":
                        return R.drawable.russia;

                    case "SEK":
                        return R.drawable.sweden;

                    case "SGD":
                        return R.drawable.singapore;

                    case "THB":
                        return R.drawable.thailand;

                    case "USD":
                        return R.drawable.united_states_of_america;

                    case "ZAR":
                        return R.drawable.south_africa;

        }
        return android.R.drawable.stat_notify_error;
    }
}
