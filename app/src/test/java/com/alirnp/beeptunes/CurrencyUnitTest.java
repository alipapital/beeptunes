package com.alirnp.beeptunes;

import org.junit.Test;

public class CurrencyUnitTest {

    @Test
    public void addition_isCorrect() {
        double excepted = 3.986;
        double AUD = 1.588;
        double BGN = 1.993;

        double inputAUD = 3.176;

        double scaleEUR = inputAUD / AUD; //2
        double result = BGN * scaleEUR;

        System.out.println(scaleEUR);
        System.out.println(result);
    }

    @Test
    public void addition_isCorrect2() {
        double AUD = 1.588;
        double BGN = 1.993;

        double inputAUD = 2;

        double scaleEUR = inputAUD / AUD; //2
        double result = BGN * scaleEUR;

        System.out.println(scaleEUR);
        System.out.println(result);
    }

}